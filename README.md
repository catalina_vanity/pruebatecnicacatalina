# Prueba Tecnica Catalina Gonzalez
## Pasos para ejecucion de seeder y migrates de la aplicación

## Instalación
Requiere composer, node, PHP 7.4  y npm

correr los migrates y seeders sin docker 
Luego de clonar el proyecto dirigirse a la ruta raiz y ejecutar los sigueintes comandos

```sh
php artisan migrate
php artisan db:seed
```

Luego ejecutar el siguiente comando para la instalacionde las dependencias y ejecucion en un entorno local

```sh
npm install
composer install
php artisan key:generate
php artisan run serve
```

para la ejecucion en contenedor docker
Nota: requiere docker-compose
```sh
docker-compose build app
docker-compose up -d
docker-compose exec app npm install
docker-compose exec app composer install
docker-compose exec app php artisan key:generate
```