<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\UserMenu;
use App\Models\Menu;

class UserMenuTest extends TestCase
{
    /**
     * test para verificar la relacion entre usuario menu y menu
     *
     * @return void
     */
    public function test_user_menu_belong_menu()
    {
        $userMenu=UserMenu::first();
        $this->assertInstanceOf(Menu::class,$userMenu->menu);
    }
}
