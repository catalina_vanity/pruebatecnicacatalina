<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Models\RolPermisos;
use Illuminate\Database\Eloquent\Collection;

class UserTest extends TestCase
{
    /**
     * test para validar la asignacion de rol y permisos a usuario
     *
     * @return void
     */
    public function test_user_rol_permisos()
    {
        $user=User::first();
        $this->assertInstanceOf(RolPermisos::class,$user->rolPermisos);
    }

    /**
     * test para validar la asignacion de menu a usuario
     *
     * @return void
     */
    public function test_user_menu()
    {
        $user=User::first();
        $this->assertInstanceOf(Collection::class,$user->userMenu);
    }
}
