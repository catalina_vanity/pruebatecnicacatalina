@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Documentos - contratos</h2>
    <p>Contratos Historicos:</p>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#documentoNuevo">Subir documento</button>
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Usuario</th>
          <th>Nombre documento</th>
          <th>Documento</th>
        </tr>
      </thead>
      <tbody>
          @foreach ($docs as $doc)
            <tr>
                <td>{{$doc->user->name}}</td>
                <td>{{$doc->doc_name}}</td>
                <td>
                   <a href="{{$doc->document_path}}" download>
                     <span class="glyphicon glyphicon-download-alt"></span>
                   </a>
                </td>
            </tr>
          @endforeach
      </tbody>
    </table>
    <div id="documentoNuevo" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Subir un nuevo documento</h4>
            </div>
            <div class="modal-body">
              <p>Por favor agrege su nuevo archivo</p>
              <input type="file" name="documento" id="documento" class="form-control">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success" onclick="sendDoc()" >Crear</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal" >Close</button>
            </div>
          </div>

        </div>
      </div>
</div>
<script>
    window.CSRF_TOKEN = '{{ csrf_token() }}';
    function sendDoc()
    {
        var formData = new FormData();
        var fileField = document.querySelector("input[type='file']");
        console.log(fileField.files[0]);
        formData.append('doc_name', fileField.files[0].name);
        formData.append('doc', fileField.files[0]);

        fetch('{{route('contratosCrear')}}',{
        headers: {
        'X-CSRF-TOKEN': window.CSRF_TOKEN
        },
        method: 'POST',
        body: formData
        })
        .then(response => response.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response));
        window.location.reload(true);
    }
</script>
@endsection
