<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Catalina Gonmzalez</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.dark\:text-gray-500{--tw-text-opacity:1;color:#6b7280;color:rgba(107,114,128,var(--tw-text-opacity))}}
        </style>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body class="antialiased">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
              <div class="navbar-header">
                <a class="navbar-brand" href="/">Catalina Gonzalez</a>
              </div>
              <ul class="nav navbar-nav">
                @auth
                    @if (isset($menus) && isset($subMenus))
                        @foreach ($menus as $item)
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{$item->menu->menu_name}}
                            <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                            @foreach ($subMenus as $submenu)
                                @if ($item->menu->id==$submenu->subMenu->menu_id)
                                    <li><a href="{{url('/')}}/{{$submenu->subMenu->path}}">{{$submenu->subMenu->sub_menu_name}}</a></li>
                                @endif
                            @endforeach
                            </ul>
                        </li>
                        @endforeach
                    @endif
                @endauth
              </ul>
              @if (Route::has('login'))
                <ul class="nav navbar-nav navbar-right">
                    @auth
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <span class="glyphicon glyphicon-user"></span> {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @else
                            <li><a href="{{ route('login') }}"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                        @if (Route::has('register'))
                            <li><a href="{{ route('register') }}"><span class="glyphicon glyphicon-user"></span> Registrarse</a></li>
                        @endif
                    @endauth
                </ul>
            @endif
            </div>
          </nav>
          <br>
          <br>
          <div class="container bg-gradient-secondary ">
            <h3 class="">
                Bienvenido
            </h3>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tincidunt, elit ac dictum efficitur, libero nisi iaculis ligula, ut semper erat ligula ut lorem. Nullam efficitur metus lectus, id iaculis dui porttitor a. Phasellus non magna quis ligula pellentesque iaculis. Donec sit amet nibh vel massa faucibus dapibus a sed odio. Pellentesque consectetur sit amet tellus ac hendrerit. Proin consectetur mauris ac lacus gravida varius. Morbi congue mi quis dictum pretium. Duis a ullamcorper purus. Donec sed congue urna, id iaculis nibh. Sed quis feugiat nisi. Proin sed cursus libero, sit amet commodo mauris. Fusce sit amet pretium arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
            </p>
            <p>
                Donec et lobortis nunc. Phasellus eu tellus vel nulla laoreet lacinia. Morbi leo nisl, egestas vitae vestibulum sed, dapibus eu erat. Vestibulum gravida sem vel libero porta, non pulvinar tellus semper. Aliquam finibus quis felis ut posuere. Etiam rhoncus nisl eu eros rutrum, vel commodo dui tempor. Pellentesque suscipit pharetra augue et euismod. Quisque in quam ante. Vestibulum sit amet lectus venenatis, aliquam erat a, rutrum metus. Duis magna risus, feugiat at scelerisque ut, eleifend vel ex. Cras mauris neque, sagittis eu imperdiet sed, vulputate quis orci. Nunc bibendum fermentum ultricies. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vestibulum ultricies erat sed urna rhoncus, quis euismod nunc pharetra. Curabitur viverra vulputate arcu, quis viverra erat facilisis non. Phasellus ac velit ligula.
            </p>
            <p>
            Donec tortor arcu, sodales et sodales commodo, iaculis sit amet mi. Donec aliquam elementum libero, id blandit ante maximus vitae. Cras ut velit at enim commodo pharetra. Suspendisse quam erat, fermentum ut feugiat eu, tristique ornare neque. Cras dapibus, eros et lobortis bibendum, dui nibh maximus diam, id lacinia justo lectus quis nunc. Vivamus tempus viverra lacus. Quisque eu nisl elit.
            </p>
            <p>
            Phasellus pulvinar imperdiet eros convallis scelerisque. Morbi eu massa purus. Fusce tortor odio, porttitor rhoncus mattis et, bibendum eu sapien. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur volutpat interdum turpis nec facilisis. Cras dui est, pulvinar vel massa vitae, pharetra posuere turpis. Mauris in nibh elementum, aliquet velit eget, euismod ex. Aenean lacinia, leo ac eleifend feugiat, est tellus ultricies sapien, ac sodales magna tellus ut lorem. Nunc varius lectus lorem, vitae semper risus viverra eget. Donec sollicitudin justo a elementum lacinia. Donec auctor, mauris non facilisis pharetra, turpis risus fermentum nulla, non vestibulum risus nibh eu ligula. Cras nibh turpis, tincidunt id porta nec, euismod id tortor.
            </p>
            <p>
            Nullam pellentesque, ipsum eget aliquam interdum, justo odio tincidunt turpis, vel scelerisque erat urna volutpat enim. Suspendisse imperdiet porta lorem, rhoncus luctus nibh consectetur quis. Sed felis turpis, molestie sodales pharetra id, venenatis in sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce egestas elementum porttitor. Nunc purus dolor, pellentesque eget tortor eu, pretium pulvinar tortor. Nunc feugiat at dolor eu dignissim. Phasellus odio velit, suscipit id orci eget, blandit ultricies nulla. Sed bibendum lacus ac lectus lacinia sagittis. Donec non commodo justo. Mauris dapibus nunc sed felis volutpat venenatis. Aenean non sollicitudin erat. Donec ac mattis mi. Sed laoreet ipsum nisl, ut finibus urna dictum vitae. Aliquam vel semper nisl, id sollicitudin ipsum.
            </p>
            <p>
            Nulla aliquet ullamcorper egestas. Phasellus id ipsum nec augue egestas pulvinar. Duis ex orci, mollis sed dapibus quis, sollicitudin nec lacus. Pellentesque sed nunc a libero aliquet accumsan eu eget leo. Duis vitae semper metus. Praesent vehicula leo urna, eget sagittis erat auctor et. Duis vitae nisi sed est ultrices auctor. Sed ac viverra nisi. In turpis odio, semper eu sem eu, dapibus euismod tellus. Quisque ac diam mattis, laoreet arcu at, commodo neque. Vivamus leo velit, fermentum sed tempor at, viverra quis nisi. Suspendisse rhoncus aliquet tincidunt. Praesent venenatis, arcu et viverra interdum, augue nisi tristique elit, a aliquet ex turpis sit amet arcu. Vestibulum eu ultrices purus, at tincidunt nibh. Proin convallis finibus ultrices. Aliquam pellentesque ante ex, sed ultrices diam laoreet eu.
            </p>
            <p>
            Nulla bibendum rhoncus justo, eu convallis dui imperdiet eu. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc eu eros sit amet nisi efficitur tristique. Quisque tempus sapien at posuere euismod. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam lacinia diam vel sem lobortis, eget dictum est elementum. Aenean elementum, tortor vel malesuada vulputate, metus eros efficitur nisi, vel malesuada nibh eros eu velit. Integer in eros ut massa ullamcorper euismod sit amet ut ante. Aliquam id molestie nisl. Mauris vestibulum hendrerit sodales. Etiam vehicula lacus ut elit cursus, vel dignissim eros efficitur. Integer sed rutrum ligula. Aliquam erat volutpat. Suspendisse fringilla lorem eget felis maximus, non sagittis enim ornare.
            </p>
            <p>
            In in ex id odio vulputate accumsan. Nunc a mattis leo, sed imperdiet enim. Donec sodales nisl a tincidunt porta. Sed congue erat non nisl faucibus, vitae venenatis neque facilisis. Curabitur elementum arcu in ipsum ullamcorper maximus. Nam eget enim eleifend, rutrum nulla ut, euismod neque. Sed arcu turpis, cursus vehicula nisi id, scelerisque iaculis diam. Curabitur mi felis, accumsan a sapien at, bibendum semper diam. Fusce pellentesque turpis leo, at malesuada mauris hendrerit eget. Quisque ullamcorper ante ac vulputate laoreet. Ut suscipit suscipit lectus quis scelerisque.
            </p>
            <p>
            Sed interdum augue diam, in ornare orci euismod id. Morbi molestie quam ac lacus malesuada elementum. In hac habitasse platea dictumst. Nulla in dignissim nibh. Nulla in nulla placerat, fermentum mauris vitae, iaculis leo. Nunc nec enim tellus. Aenean metus nulla, ullamcorper non nibh eu, tempus viverra sem. Curabitur ligula mauris, facilisis ut accumsan eu, vestibulum vel dolor. Ut convallis erat sed erat interdum vestibulum. Duis vel urna ac sapien sodales sagittis.
            </p>
            <p>
            Nunc et nulla eros. Pellentesque sem elit, porttitor eget faucibus ut, dapibus in enim. Nunc facilisis, est facilisis facilisis blandit, leo nibh consequat nibh, vel tincidunt dui turpis ac nisl. Curabitur at tortor nulla. Vestibulum bibendum rutrum pretium. Quisque eleifend magna eu urna scelerisque, vel maximus orci auctor. Vivamus scelerisque eros erat, sit amet laoreet ex ultrices non. Nunc luctus, ipsum in tempor iaculis, sem tortor tincidunt nisl, quis luctus turpis lacus nec dui. Morbi non odio feugiat, vulputate lectus eu, malesuada nisi. In gravida urna et neque eleifend pulvinar. Sed nibh neque, fringilla efficitur magna in, pharetra porta lorem. Maecenas dictum nibh sed fermentum scelerisque.
            </p>
            <br>
            <img src="../img/original.png" class="img-fluid" width="100%">
          </div>
    </body>
</html>
