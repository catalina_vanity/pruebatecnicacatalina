@extends('layouts.app')

@section('content')
<div class="container">
    @php
        //dd($userList);
    @endphp
    <h3>
        Bienvenido!
    </h3>
    <p>
        seleccione el usuario al que desea modificar sus permisos:
    </p>
    <form action="{{route('permisosUpdate')}}" method="POST" id="formPermisdos">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div class="row">
            <div class="col-sm-3">
                <label for="user">Usuario</label>
                <select name="user" id="user" class="form-control">
                    <option value="0">Seleccione...</option>
                    @foreach ($userList as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-4" id="permisos" hidden>
                @php
                    $userSelected=0;
                @endphp

                <label >Permisos</label>
                <br>

                <label class="checkbox-inline"><input type="checkbox" id="view" name="view" value="view" >View</label>
                <label class="checkbox-inline"><input type="checkbox" id="delete" name="delete" value="delete"    >Delete</label>
                <label class="checkbox-inline"><input type="checkbox" id="update" name="update" value="update"    >Update</label>
                <label class="checkbox-inline"><input type="checkbox" id="create" name="create" value="create"    >Create</label>
            </div>
        </div>
        <br>
        <div class="row" id="save" hidden>
            <div class="col-sm-1">
                <button type="button" class="btn btn-success" onclick="formsubmit()">Guardar</button>
            </div>
        </div>
    </form>

</div>
<script>
    var users = document.getElementById("user");
    var permisos= document.getElementById("permisos");
    var save= document.getElementById("save");

    users.addEventListener("change", function() {
    if(users.value !=0)
    {
        permisos.hidden=false;
        save.hidden=false;
    }
    });
    function formsubmit()
    {
        var form= document.getElementById("formPermisdos");
        form.submit();

    }
</script>
@endsection
