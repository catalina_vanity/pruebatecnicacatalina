<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    use HasFactory;
    protected $table='rol';
    /**
     * The attributes that are mass assignable.
     * @var string[]
     */
     protected $fillable = [
       'role_name'
   ];


   public function rolPermisos()
   {
       return $this->belongsTo(RolPermisos::class);
   }
}
