<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSubMenu extends Model
{
    use HasFactory;
    protected $table='user_sub_menu';
    /**
     * The attributes that are mass assignable.
     * @var string[]
     */
     protected $fillable = [
       'sub_menu_id',
       'user_id'
   ];


   public function subMenu()
   {
       return $this->belongsTo(SubMenu::class);
   }
   public function user()
   {
       return $this->belongsTo(User::class);
   }
}
