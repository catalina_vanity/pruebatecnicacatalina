<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolPermisos extends Model
{
    use HasFactory;
    protected $table='rol_permisos';
    /**
     * The attributes that are mass assignable.
     * @var string[]
     */
     protected $fillable = [
       'view',
       'delete',
       'create',
       'update',
       'role_id'
   ];


   public function rol()
   {
       return $this->belongsTo(Rol::class,'role_id');
   }
}
