<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDocument extends Model
{
    use HasFactory;
    protected $table='user_document';
    /**
     * The attributes that are mass assignable.
     * @var string[]
     */
     protected $fillable = [
       'user_id',
       'document_path',
       'doc_name'
   ];


   public function user()
   {
       return $this->belongsTo(User::class);
   }
}
