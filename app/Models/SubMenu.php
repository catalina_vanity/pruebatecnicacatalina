<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubMenu extends Model
{
    use HasFactory;
    protected $table='sub_menu';
    /**
     * The attributes that are mass assignable.
     * @var string[]
     */
     protected $fillable = [
       'menu_id',
       'sub_menu_name'
   ];


   public function menu()
   {
       return $this->hasMany(Menu::class);
   }
}
