<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserMenu;
use App\Models\UserSubMenu;

class VentasController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()){
            $user=Auth::user();
            $menus=UserMenu::where('user_id',$user->id)->get();
            $subMenu=UserSubMenu::where('user_id',$user->id)->get();
            return view('ventas.ejecutivos',['menus' => $menus,
            'subMenus' => $subMenu]);
        }
        return view('ventas.ejecutivos');
    }
}
