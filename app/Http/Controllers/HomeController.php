<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserMenu;
use App\Models\UserSubMenu;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()){
            $user=Auth::user();
            $menus=UserMenu::where('user_id',$user->id)->get();
            $subMenu=UserSubMenu::where('user_id',$user->id)->get();
            return view('welcome',['menus' => $menus,
            'subMenus' => $subMenu]);
        }
        return view('welcome');
    }
}
