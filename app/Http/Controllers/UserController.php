<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserMenu;
use App\Models\UserSubMenu;
use App\Models\User;

class UserController extends Controller
{

     /**
     * Show the permisos view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function permisos()
    {
        if(Auth::user()){
            $user=Auth::user();
            $menus=UserMenu::where('user_id',$user->id)->get();
            $subMenu=UserSubMenu::where('user_id',$user->id)->get();
            $userList=User::all();
            return view('auth.permisos',['menus' => $menus,
            'subMenus' => $subMenu, 'userList' => $userList]);
        }
        return view('login');
    }
     /**
     * Show the config menu view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function confgMenu()
    {
        if(Auth::user()){
            $user=Auth::user();
            $menus=UserMenu::where('user_id',$user->id)->get();
            $subMenu=UserSubMenu::where('user_id',$user->id)->get();
            $userList=User::all();
            return view('auth.permisos',['menus' => $menus,
            'subMenus' => $subMenu, 'userList' => $userList]);

        }
        return view('login');
    }

    /**
     *Permite la configuracion de los permisos de un usuario
     *
     *@param Request $permisos
     *@return boolen
     */
    public function confgPermiso(Request $request)
    {
        $user=User::where('id',$request->user)->first();
        $permisos=$user->rolPermisos;
        $permisos->view=isset($request->view) ? true:false;
        $permisos->delete=isset($request->delete) ? true:false;
        $permisos->update=isset($request->update) ? true:false;
        $permisos->create=isset($request->create) ? true:false;
        $permisos->updated_at=date("Y-m-d H:i:s");
        $permisos->save();
        return redirect('/home');
    }
}
