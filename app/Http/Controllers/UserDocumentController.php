<?php

namespace App\Http\Controllers;

use App\Models\UserDocument;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateUserDocumentRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\UserMenu;
use App\Models\UserSubMenu;


class UserDocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()){
            $user=Auth::user();
            $menus=UserMenu::where('user_id',$user->id)->get();
            $subMenu=UserSubMenu::where('user_id',$user->id)->get();

            if($user->rolPermisos->rol->role_name=='admin')
            {
                $docs=UserDocument::all();
            }else{
                $docs=UserDocument::where('user_id',$user->id)->get();
            }

            return view('documentos.contratos',['menus' => $menus,
            'subMenus' => $subMenu,'docs' => $docs]);
        }
        return view('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserDocumentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->file('doc')->storeAs('public',$request->doc_name);
            $url=asset('storage/'.$request->doc_name);
            $doc=UserDocument::create([
                'user_id' => Auth::user()->id,
                'document_path' => $url,
                'doc_name' => $request->doc_name,
            ]);
            return response(json_encode($doc),200);
        } catch (\Throwable $th) {
            return response('error interno',500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserDocument  $userDocument
     * @return \Illuminate\Http\Response
     */
    public function show(UserDocument $userDocument)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserDocument  $userDocument
     * @return \Illuminate\Http\Response
     */
    public function edit(UserDocument $userDocument)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserDocumentRequest  $request
     * @param  \App\Models\UserDocument  $userDocument
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserDocumentRequest $request, UserDocument $userDocument)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserDocument  $userDocument
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserDocument $userDocument)
    {
        //
    }
}
