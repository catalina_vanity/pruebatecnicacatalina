<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rol')->insert([
            'role_name' => 'user',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('rol')->insert([
            'role_name' => 'admin',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
