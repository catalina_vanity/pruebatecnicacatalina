<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sub_menu')->insert([
            'menu_id' =>1,
            'sub_menu_name' => 'Ejecutivos',
            'path' => 'ventas/ejecutivos/',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('sub_menu')->insert([
            'menu_id' =>2,
            'sub_menu_name' => 'Contratos',
            'path' => 'documentacion/contratos/',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('sub_menu')->insert([
            'menu_id' =>2,
            'sub_menu_name' => 'Protocolos',
             'path' => 'documentacion/protocolos/',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('sub_menu')->insert([
            'menu_id' =>3,
            'sub_menu_name' => 'Permisos',
            'path' => 'usuarios/permisos/',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
