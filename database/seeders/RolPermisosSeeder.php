<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolPermisosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rol_permisos')->insert([
            'role_id' => 2,
            'delete' => true,
            'view' =>true,
            'update' => true,
            'create' => true,
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
