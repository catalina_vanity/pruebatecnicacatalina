<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->insert([
            'menu_name' => 'Ventas',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('menu')->insert([
            'menu_name' => 'Documentacion',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('menu')->insert([
            'menu_name' => 'Usuarios',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

    }
}
