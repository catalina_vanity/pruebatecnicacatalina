<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSubMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_sub_menu', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('sub_menu_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('sub_menu_id')->references('id')->on('sub_menu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_sub_menu');
    }
}
