<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/ventas/ejecutivos', [App\Http\Controllers\VentasController::class, 'index'])->name('ejecutivos')->middleware('auth', 'menu');
Route::get('/documentacion/contratos', [App\Http\Controllers\UserDocumentController::class, 'index'])->name('contratos')->middleware('auth', 'menu');
Route::post('/documentacion/contratos', [App\Http\Controllers\UserDocumentController::class, 'store'])->name('contratosCrear')->middleware('auth', 'contratosCrear');
Route::get('/documentacion/protocolos', [App\Http\Controllers\VentasController::class, 'index'])->name('protocolos')->middleware('auth', 'menu');
Route::get('/usuarios/permisos', [App\Http\Controllers\UserController::class, 'permisos'])->name('permisos')->middleware('auth', 'menu');
Route::post('/usuarios/permisos', [App\Http\Controllers\UserController::class, 'confgPermiso'])->name('permisosUpdate')->middleware('auth', 'userUpdate');
